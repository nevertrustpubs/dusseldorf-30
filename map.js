
var map = L.map('map').setView([51.505, -0.09], 13);

//Karte
var tile = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

L.Control.geocoder().addTo(map);

//Karte
var laermkarte_nacht = L.tileLayer.wms('http://maps.duesseldorf.de/services/laerm/ows?',
{
    layers: 'laermkarte_nacht',
    format: 'image/png',
	transparent: true
});

laermkarte_nacht.options.crs= L.CRS.EPSG4326;

//Karte
var laermkarte_tag = L.tileLayer.wms('http://maps.duesseldorf.de/services/laerm/ows?',
{
    layers: 'laermkarte_tag',
    format: 'image/png',
	transparent: true
});

laermkarte_tag.options.crs= L.CRS.EPSG4326;
var overlayMaps = {
    "Lärm Tag": laermkarte_tag,
    "Lärm Nacht": laermkarte_nacht
};

//Standardauswahl
laermkarte_nacht.addTo(map);

var layerControl = L.control.layers(overlayMaps).addTo(map);

map.panTo(new L.LatLng(51.2277, 6.7735));